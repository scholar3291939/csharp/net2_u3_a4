using System;
using System.Collections.Generic;

namespace net2_u3_a4 {

    public class Aminoacido
    {
        //Diccionario que contiene la información de los aminoácidos
        private static Dictionary<string, string[]> _aminoacidos = new Dictionary<string, string[]>()
            {
                {"ALA", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"ARG", new [] {"Polar", "Cargado Positivo", "No Hidrofóbico"}},
                {"ASN", new [] {"Polar", "No Cargado", "No Hidrofóbico"}},
                {"ASP", new [] {"Polar", "Cargado Negativo", "No Hidrofóbico"}},
                {"CYS", new [] {"Polar", "No Cargado", "Hidrofóbico"}},
                {"GLN", new [] {"Polar", "No Cargado", "No Hidrofóbico"}},
                {"GLU", new [] {"Polar", "Cargado Negativo", "No Hidrofóbico"}},
                {"GLY", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"HIS", new [] {"Polar", "Cargado Positivo", "No Hidrofóbico"}},
                {"ILE", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"LEU", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"LYS", new [] {"Polar", "Cargado Positivo", "No Hidrofóbico"}},
                {"MET", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"PHE", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"PRO", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"SER", new [] {"Polar", "No Cargado", "No Hidrofóbico"}},
                {"THR", new [] {"Polar", "No Cargado", "No Hidrofóbico"}},
                {"TRP", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
                {"TYR", new [] {"Polar", "No Cargado", "No Hidrofóbico"}},
                {"VAL", new [] {"No Polar", "No Cargado", "Hidrofóbico"}},
            };

        //Propiedades de la clase
        public string Nombre { get; set; }
        public string Polaridad { get; set; }
        public string Carga { get; set; }
        public string Hidrofilicidad { get; set; }

        //Obtiene la polaridad de un aminoácido
        public static string ObtenerPolaridad(string nombre)
        {
            if (!_aminoacidos.ContainsKey(nombre))
            {
                throw new ArgumentException("El aminoácido no existe: " + nombre);
            }

            return _aminoacidos[nombre][0];
        }

        //Obtiene la carga de un aminoácido
        public static string ObtenerCarga(string nombre) {
            if (!_aminoacidos.ContainsKey(nombre))
            {
                throw new ArgumentException("El aminoácido no existe: " + nombre);
            }

            return _aminoacidos[nombre][1];
        }

        //Obtiene la carga de un aminoácido
        public static string ObtenerHidrofilidad(string nombre)
        {
            if (!_aminoacidos.ContainsKey(nombre))
            {
                throw new ArgumentException("El aminoácido no existe: " + nombre);
            }

            return _aminoacidos[nombre][2];
        }

        public static Aminoacido GetInstance(string nombre) {
            try
            {
                Aminoacido newObject = new Aminoacido();
                newObject.Nombre = nombre;
                newObject.Polaridad = ObtenerHidrofilidad(nombre);
                newObject.Hidrofilicidad = ObtenerPolaridad(nombre);
                newObject.Carga = ObtenerCarga(nombre);
                return newObject;
            }
            catch (ArgumentException) {
                return null;
            }

        }

        public static bool Existe(string nombre)
        {
            if (!_aminoacidos.ContainsKey(nombre))
            {
                throw new ArgumentException("El aminoácido no existe: " + nombre);
            }

            return true;
        }

    }
}


