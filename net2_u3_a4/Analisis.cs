﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace net2_u3_a4
{
    public class Analisis : AnalisisBase
    {
        public string patronAminoacidos;
   
        public List<Aminoacido?> listAminoacidos;

        public Analisis(string cadenaPatron)
            : base()
        { 
            patronAminoacidos = LimpiarCadena(cadenaPatron); // Limpieza de la cadena
            listAminoacidos = ConvertirEstructuraCadena(patronAminoacidos); // Convierte a lista de objetos

        }

        public void AnalizaPatron() {

            if (!listAminoacidos.Contains(null))
            {
                Console.WriteLine("El patron parece estar correcto. Todos los aminoacidos fueron encontrados\n");
                foreach (Aminoacido? aminoacido in listAminoacidos) {
                    Console.Write(aminoacido.Nombre + "-");
                }
                
            }
            else {
                Console.WriteLine("El patron contiene elementos incorrectos");
                foreach (Aminoacido? aminoacido in listAminoacidos)
                {
                    if (aminoacido == null)
                    {
                        Console.Write("ERROR" + "-");
                    }
                    else {
                        Console.Write(aminoacido.Nombre + "-");
                    }
                }
            }
            Console.WriteLine("");
            int x = 1;
            foreach (Aminoacido? aminoacido in listAminoacidos) {
                if (aminoacido != null)
                {
                    Console.WriteLine($"Posicion [{x}] Nombre:{aminoacido.Nombre} Carga:{aminoacido.Carga} Hidrofilidad:{aminoacido.Hidrofilicidad}");
                }
                else {
                    Console.WriteLine($"Posicion [{x}] Error de entrada");
                }
                
            }


        }

        public void CalculaCarga()
        {
            Dictionary<string, string> r = CalculaPorcentajesBase(listAminoacidos, a => a.Carga);
            MostrarResultados(r, "Carga");
        }
    
        public void CalculaHidrofilidad()
        {
            Dictionary<string, string> r = CalculaPorcentajesBase(listAminoacidos, a => a.Hidrofilicidad);
            MostrarResultados(r, "Hidrofilidad");

        }
   
        public void CalculaPolaridad()
        {
            Dictionary<string, string> r = CalculaPorcentajesBase(listAminoacidos, a => a.Polaridad);
            MostrarResultados(r, "Polaridad");
        }
    
        public bool IdentificaSecuenciaUnionLigando() {
            // Se regresa true solo si la lista de aminos es igual a 7, el 4 elemento es GLY 
            // Y todos los elementos son aminos, no contiene errores en el ingreso de datos.

            if (listAminoacidos.Count == 7)
            {
                foreach (Aminoacido amino in listAminoacidos)
                {
                    if (amino == null) {
                        return false;
                    }
                }
                if (listAminoacidos[3] != null)
                {
                    if (listAminoacidos[3].Nombre.Equals("GLY")){
                        return true;
                    }
                }
            }
            return false;
        }
   
        public bool IdentificaPatronSenal()
        // Si la lista es menor a 3, el elemento 2 o 3 son null o no son SER y ARG en las posiciones 2 y 3 
        // respectivamente se regresa false, de lo contrario se regresa true
        {
            if (listAminoacidos.Count >= 3) {
                if (listAminoacidos[1] != null) {
                    if (listAminoacidos[2] != null) {
                        if (listAminoacidos[1].Nombre.Equals("SER") & listAminoacidos[2].Nombre.Equals("ARG")) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public void EvaluarPatrones() {
            if (IdentificaPatronSenal())
            {
                Console.WriteLine("Se ha identificado un patron de senal en la serie de aminoacidos que has ingresado");
            }
            else {
                Console.WriteLine("No se ha identificado un patron de senal en la cadena");
            }

            if (IdentificaSecuenciaUnionLigando())
            {
                Console.WriteLine("Se ha identificado Secuencia de union a ligando en la serie que ingresaste");
            }
            else {
                Console.WriteLine("No se ha identificado una Secuencia de union a ligando en la seria");
            }

        }


    }
}
