﻿
namespace net2_u3_a4
{
    class Program {


        public static Analisis? analisis = null;

        public static void Main(string[] args)
        {
            try
            {

                StartProgram();

            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Ocurrio un error en el programa");
                Console.WriteLine(ex.Message);
            }
        }

        public static void StartProgram() {
            string menu = "Bienvenido al programa de analisis de aminoacidos" +
                "\nPara analizar las cadenas de aminoacidos ingresa el valor de la cadena" +
                "\nAsegurate que ingresar la cadena en el siguiente formato:" +
                "\n-Amin-Amin-Amin-  <-Considera los guines como ligaduras para cada aminoacido" +
                "\nEs posible analizar cadenas de cualquier dimension." +
                "\nSelecciona una de las siguientes opciones:\n" +
                "\n[1] Ingresar el valor de una cadena de aminoacidos" +
                "\n[2] Evaluar los patrones de la cadena de aminoacidos" +
                "\n[3] Mostrar las estadisticas de la cadena de aminoacidos" +
                "\n[4] Mostrar la cadena ingresada (validarla)" +
                "\n[5] Salir del programa"
                ;

            while (true) {
                Console.Clear();
                Console.WriteLine(menu);
                EjecutaOpcion(LeerOpcion());
                Thread.Sleep(3000);
            }
        }

        public static void EjecutaOpcion(int opcion) { 
            switch(opcion)
            {
                case 1:
                    {
                        Console.WriteLine("Ingresa una cadena de aminoacidos para ser interpretada");
                        Console.Write("Recuerda emplear el siguiente formato: Amin-Amin-Amin: ");

                        string cadena = Console.ReadLine();
                        analisis = new Analisis(cadena);
                        break;
                    }
                case 2:
                    {
                        if (!AnalisisInicializado()) {
                            Console.WriteLine("No hay informacion para analizar, utiliza la opcion 1 para cargar datos primero");
                            break;
                        }
                        analisis.EvaluarPatrones();
                        break;
                    }
                case 3:
                    {
                        if (!AnalisisInicializado())
                        {
                            Console.WriteLine("No hay informacion para analizar, utiliza la opcion 1 para cargar datos primero");
                            break;
                        }
                        analisis.CalculaCarga();
                        analisis.CalculaPolaridad();
                        analisis.CalculaHidrofilidad();
                        break;
                    }
                case 4:
                    {
                        if (!AnalisisInicializado())
                        {
                            Console.WriteLine("No hay informacion para analizar, utiliza la opcion 1 para cargar datos primero");
                            break;
                        }
                        analisis.AnalizaPatron();
                        break;
                    }
                case 5:
                    {
                        Console.WriteLine("Saliendo del program");
                        Environment.Exit(0);
                        break;
                    }
                case -1:
                    {
                        Console.WriteLine("El valor ingresado no fue reconocido como una de las opciones de programa" +
                        "\nIntenta ingresar un valor del menu que sea valido");
                        break;
                    }

            }
            
        }

        public static int LeerOpcion()
        {
            try {
                string opcionStr = Console.ReadLine();
                int opcionInt = int.Parse(opcionStr);
                if (opcionInt <= 5 & opcionInt>=1) {
                    return opcionInt;
                }
                return -1;
            }catch(Exception ex)
            {
                return -1;
            }
        }

        public static bool AnalisisInicializado() {
            if (analisis == null) {
                return false;
            }
            return true;
        }

    }
}