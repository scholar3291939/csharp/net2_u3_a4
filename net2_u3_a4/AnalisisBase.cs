﻿namespace net2_u3_a4
{
    public class AnalisisBase
    {

        protected static List<Aminoacido?> ConvertirEstructuraCadena(string cadena)
        // Recibe como parametro el string que representa las cadenas de aminacidos
        // Limpia y convierte la cadena de modo que pueda ser interpretada, separando
        // los elementos por "-"
        // Regresa una lista de objetos de tipo Aminoacidos, en caso de no encontrar
        // la definicion del aminacido se agrega el elemento en la posicion correspondiente
        // de la lista con un objeto null.
        {
            cadena = LimpiarCadena(cadena); // Sin guiones a los extremos y en mayusculas
            List<Aminoacido?> listAminoacidos = new List<Aminoacido?>();

            foreach (string elemento in cadena.Split('-'))
            {
                try
                {
                    listAminoacidos.Add(Aminoacido.GetInstance(elemento));
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine("Ocurrio el siguiente error " + ex.ToString());
                }
            }
            return listAminoacidos;

        }
     
        protected static string LimpiarCadena(string cadena)
        // Limpia la cadena de texto que contiene la cadena de aminoacidos
        // La convierte en mayusculas
        {
            if (cadena.StartsWith("-"))
            {
                return LimpiarCadena(cadena.Substring(1));  // Llama recursivamente eliminando el primer carácter
            }
            if (cadena.EndsWith("-"))
            {
                return LimpiarCadena(cadena.Substring(0, cadena.Length - 1)); // Llama recursivamente eliminando el último carácter
            }
            return cadena.ToUpper();  // Si la cadena no tiene guiones en los extremos, devolverla tal cual
        }

        protected Dictionary<string, string> CalculaPorcentajesBase(List<Aminoacido?> listaAminoacidos, Func<Aminoacido, string> propiedad) 
        // listaAminoacidos : Lista de aminoacidos con posibles elementos de tipo null
        // propiedad : Nombre de la propiedad de los objetos que debe ser analizada
        // Calcula los porcentajes para cada una de las propiedades que puede tener el objeto de tipo Aminoacido
        // El metodo debe ser reutilizado con las propiedades de tipo string para calcular la cantidad de incidencias que aparecen
        // contra el total de objetos que se presentan, sin tomar en consideracion lo objetos que aparezcan de tipo null
        {
            int totalIncidencias = listaAminoacidos.Count; // Total de elementos de la lista

            Dictionary<string, double> conteoPropiedad = new Dictionary<string, double>(); // Conteo para cada propiedad analizada
            Dictionary<string, string> resultados = new Dictionary<string, string>(); // Conteo para cada propiedad analizada

            foreach (Aminoacido? aminoacido in listaAminoacidos)
            {
                if (aminoacido == null) {
                    if (conteoPropiedad.ContainsKey("ERRORES"))
                    {
                        conteoPropiedad["ERRORES"]++;
                    }
                    else
                    {
                        conteoPropiedad["ERRORES"] = 1;
                    }
                    continue; // No entra al siguiente bloque porque el objeto null no posee las propiedades buscadas
                }

                string propAminoacido = propiedad(aminoacido);
                if (conteoPropiedad.ContainsKey(propAminoacido))
                {
                    conteoPropiedad[propAminoacido]++; // Suma una cuenta al valor de la propiedad encontrado
                }
                else
                {
                    conteoPropiedad[propAminoacido] = 1; // Inicializa el valor de la propiedad con 1, luego se iran sumando ++
                }
            }

            foreach (string k in conteoPropiedad.Keys) {
                double r = (conteoPropiedad[k] / listaAminoacidos.Count) * 100;
                resultados[k] = $"Se presentan [{conteoPropiedad[k]}/{listaAminoacidos.Count}] {r}% del tipo {k}";
            }

            return resultados;
        }

        protected void MostrarResultados(Dictionary<string, string> resultados, string tipoAnalisis)
        {
            Console.WriteLine($"\n\nEl analisis de {tipoAnalisis} muestra lo siguiente:");
            foreach (string k in resultados.Keys)
            {
                Console.WriteLine(resultados[k]);
            }
        }
    }
}